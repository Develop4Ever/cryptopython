#!/usr/bin/env python
# -*- coding: utf-8 -*-

import string

def translate(testo, codifica, mode):


    codifica = codifica
    testo = testo
    mode = mode

    #--- Rot13 ----------------------------------------------------------------
    rot13_alfa = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    rot13_beta = "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm"
    if mode == 1:
        rot13_table = string.maketrans(rot13_alfa, rot13_beta)
    if mode == 2:
        rot13_table = string.maketrans(rot13_beta, rot13_alfa)
    #--------------------------------------------------------------------------

    #--- Cesare ---------------------------------------------------------------
    cesare_alfa = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    cesare_beta = "DEFGHIJKLMNOPQRSTUVWXYZABCdefghijklmnopqrstuvwxyzabc3456789012"
    if mode == 1:
        cesare_table = string.maketrans(cesare_alfa, cesare_beta)
    if mode == 2:
        cesare_table = string.maketrans(cesare_beta, cesare_alfa)
    #--------------------------------------------------------------------------

    #--- Albam ----------------------------------------------------------------
    albam_alfa = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    albam_beta = "LMNOPQRSTUVWXYZABCDEFGHIJKlmnopqrstuvwxyzabcdefghijk1234567890"
    if mode == 1:
        albam_table = string.maketrans(albam_alfa, albam_beta)
    if mode == 2:
        albam_table = string.maketrans(albam_beta, albam_alfa)

    #--------------------------------------------------------------------------

    #--- Carbonaro ------------------------------------------------------------
    carbonaro_alfa = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    carbonaro_beta = "OPGTIVCHEJKRNMABQLZDUFWXYSopgtivchejkrnmabqlzdufwxys"
    if mode == 1:
        carbonaro_table = string.maketrans(carbonaro_alfa, carbonaro_beta)
    if mode == 2:
        carbonaro_table = string.maketrans(carbonaro_beta, carbonaro_alfa)
    #--------------------------------------------------------------------------

    #--- Atbash ---------------------------------------------------------------
    atbash_alfa = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    atbash_beta = "ZYXWVUTSRQPONMLKJIHGFEDCBAzyxwvutsrqponmlkjihgfedcba9876543210"
    if mode == 1:
        atbash_table = string.maketrans(atbash_alfa, atbash_beta)
    if mode == 2:
        atbash_table = string.maketrans(atbash_beta, atbash_alfa)
    #--------------------------------------------------------------------------

    #--- Pizzini --------------------------------------------------------------
    pizzini = {'111':' ','a':'4', 'b':'5',
                    'c':'6', 'd':'7', 'e':'8',
                    'f':'9', 'g':'010', 'h':'011',
                    'i':'012', 'l':'013', 'm':'014',
                    'n':'015', 'o':'016', 'p':'017',
                    'q':'018', 'r':'020', 's':'021',
                    't':'022', 'u':'023', 'v':'024',
                    'z':'025', 'Z':'025'}
    #----------------------------------------------------------------------

    #--- Morse ------------------------------------------------------------
    Morse = {"a": "·-", "b": "-···", "c":  "-·-·",
                  "d": "-··" , "e": "·", "f": "··-·",
                  "g": "--·", "h": "····", "i": "··",
                  "l": "·-··", "m": "--", "n": "-·",
                  "o": "---", "p": "·--·", "q": "--·-",
                  "r": "·-·", "s": "···", "t": "-" ,
                  "u": "··-", "v": "···-", "z": "--··",
                  " ": "###", "k": "-·-", "j": "·---",
                  "w": "·--", "y": "-·--" , "x": "-··-",
                  "0": "-----", "1": "·----", "2": "··---",
                  "3": "···--", "4": "····-", "5": "·····",
                  "6": "-····", "7": "--···", "8": "---··",
                  "9":"----·", ".": "·-·-·-" , ",": "--··--",
                  ":": "---···", "?": "··--··", "!": "-·-·--" ,
                  "'": "·----.", "\"": "·-··-·" }
    #----------------------------------------------------------------------

    #--- Elaborazione testo & traduzione ----------------------------------

    if codifica == 'Cifrario di Cesare':
        return testo.translate(cesare_table)
    if codifica == 'Alfabeto Carbonaro':
        return testo.translate(carbonaro_table)
    if codifica == 'Cifrario di Albam':
        return testo.translate(albam_table)
    if codifica == 'Rot-13':
        return testo.translate(rot13_table)
    if codifica == 'Codifica Atbash':
        return testo.translate(atbash_table)
    #----------------------------------------------------------------------



