#!/usr/bin/env python
# -*- coding: utf-8 -*-

<<<<<<< HEAD
#--- Gtk import ---------------------------------------------------------------
import gtk, pygtk
#------------------------------------------------------------------------------
#--- Other import -------------------------------------------------------------
import random
import pango
import string
#------------------------------------------------------------------------------
#--- My import ----------------------------------------------------------------
import TradPy
#------------------------------------------------------------------------------

class CryptoPython():

    def __init__(self):

        #--- Function for the creation of the GUI -----------------------------
        self.TextBuffer_from = gtk.TextBuffer()
        self.TextBuffer_from.set_text('Inserisci il testo da tradurre')
        self.TextBuffer_to = gtk.TextBuffer()
        self.TextBuffer_to.set_text('Quì verrà mostrata la traduzione')
        self.TextView_from = gtk.TextView(self.TextBuffer_from)
        self.TextView_from.set_wrap_mode(gtk.WRAP_CHAR)
        self.TextView_to = gtk.TextView(self.TextBuffer_to)
        self.TextView_to.set_wrap_mode(gtk.WRAP_CHAR)
        self.ScrolledWindow_to = gtk.ScrolledWindow()
        self.ScrolledWindow_to.add(self.TextView_to)
        self.ScrolledWindow_to.show()
        self.ScrolledWindow_from = gtk.ScrolledWindow()
        self.ScrolledWindow_from.add(self.TextView_from)
        self.ScrolledWindow_from.show()
        self.Label_status = gtk.Label('')
        self.ListStore_1 = gtk.ListStore(str)
        self.ComboBoxEntry_1 = gtk.ComboBoxEntry(self.ListStore_1, 0)
        self.ComboBoxEntry_1.grab_focus()
        self.Entry_file = gtk.Entry()
        self.Entry_file.set_size_request(100, 27)
        self.Button_selectfile = gtk.Button('File')
        self.Button_selectfile.connect("clicked", self.OpenFile)
        self.Button_selectfile.set_usize(80, 10)
        self.Button_encrypt = gtk.Button('Codifica')
        self.Button_encrypt.connect("clicked", self.Codifica)
        self.Button_encrypt.set_usize(0, 0)
        self.Button_decrypt = gtk.Button('Decodifica')
        self.Button_decrypt.connect("clicked", self.Decodifica)
        self.Button_decrypt.set_usize(0, 0)
        self.HBox_1 = gtk.HBox(homogeneous = False)
        self.HBox_1.set_size_request(10, 20)
        self.HBox_1.pack_start(self.ComboBoxEntry_1)
        self.HBox_2 = gtk.HBox(homogeneous = False)
        self.HBox_2.pack_start(self.Entry_file)
        self.HBox_2.pack_start(self.Button_selectfile,expand = False)
        self.HBox_2.set_usize(0, 10)
        self.VBox_1 = gtk.VBox(homogeneous = False)
        self.VBox_1.set_size_request(20, 10)
        self.VBox_1.pack_start(self.HBox_1)
        self.VBox_1.pack_start(self.Label_status)
        self.VBox_1.pack_start(self.HBox_2)
        self.VBox_1.pack_start(self.Button_encrypt)
        self.VBox_1.pack_start(self.Button_decrypt)
        self.HBox_3 = gtk.HBox(homogeneous = False)
        self.HBox_4 = gtk.HBox(homogeneous = False)
        self.HBox_4.set_size_request(450, 200)
        self.HBox_4.pack_start(self.ScrolledWindow_from)
        self.HBox_4.pack_start(self.VBox_1)
        self.HBox_4.pack_start(self.ScrolledWindow_to)
        self.HBox_5 = gtk.HBox(homogeneous = False)
        self.VBox_2 = gtk.VBox(homogeneous = False)
        self.VBox_2.pack_start(self.HBox_3)
        self.VBox_2.pack_start(self.HBox_4)
        self.VBox_2.pack_start(self.HBox_5)
        self.Window = gtk.Window()
        self.Window.set_size_request(800, 200)
        self.Window.set_border_width(5)
        self.Window.set_resizable(gtk.FALSE)
        self.Window.add(self.VBox_2)
        self.Window.set_focus(None)
        self.Window.show_all()
        #----------------------------------------------------------------------

    def OpenFile(self, Button_selectfile):

        #--- Function for selecting the File ----------------------------------
        self.openfile = gtk.FileChooserDialog(title = "Open file .txt", action = 0, buttons = (gtk.STOCK_OPEN, 3))
        self.openfile.set_default_response(0)
        return_file = self.openfile.run()
        if return_file == 3:
            return_file = self.openfile.get_filename()
            self.Entry_file.set_text(str(return_file))
            self.openfile.destroy()
        #----------------------------------------------------------------------

    def Codifica(self, Cryptize):

        #--- Function of coding (TextInput)------------------------------------
        self.Language = str(self.ComboBoxEntry_1.get_active_text())
        self.Buffer = self.TextView_from.get_buffer()
        self.start, self.end = self.Buffer.get_bounds()
        self.Text = str(self.Buffer.get_text(self.start, self.end))
        self.TextBuffer_to.set_text(str(TradPy.translate(self.Text, self.Language, 1)))

        #--- Function o coding (File) -----------------------------------------
        self.ResultFile = []
        self.File = str(self.Entry_file.get_text())
        self.Path = []

        if self.File != "":
            self.PathSplittata = string.split(self.File, "/")
            for self.Key in self.PathSplittata:
                self.Path.append(self.Key)
            del self.Path[-1]
            self.PathCompleta = string.join(self.Path, "/")
            self.PathOutput = self.PathCompleta+"/output.txt"
            self.FileDaTradurre = open(self.File, 'r')
            for self.Word in self.FileDaTradurre:
                self.TranslatedWord = str(TradPy.translate(self.Word, self.Language, 1))
                self.ResultFile.append(self.TranslatedWord)
            self.FileDaTradurre.close()
            self.TestoTradotto = string.join(self.ResultFile, "")
            self.ResultFile = []
            self.FileTradotto = open(self.PathOutput, 'w')
            self.FileTradotto.write(self.TestoTradotto)
            self.FileTradotto.close()
        #----------------------------------------------------------------------


    def Decodifica(self, Button_decrypt):

        #--- Function of decoding (TextInput) ---------------------------------
        self.Language = str(self.ComboBoxEntry_1.get_active_text())
        self.Buffer = self.TextView_from.get_buffer()
        self.start, self.end = self.Buffer.get_bounds()
        self.Text = str(self.Buffer.get_text(self.start, self.end))
        self.TextBuffer_to.set_text(str(TradPy.translate(self.Text, self.Language, 2)))

        #--- Function o decoding (File) ---------------------------------------
        self.ResultFile = []
        self.File = str(self.Entry_file.get_text())
        self.Path = []

        if self.File != "":

            self.PathSplittata = string.split(self.File, "/")
            for self.Key in self.PathSplittata:
                self.Path.append(self.Key)
            del self.Path[-1]
            self.PathCompleta = string.join(self.Path, "/")
            self.PathOutput = self.PathCompleta+"/output.txt"
            self.FileDaTradurre = open(self.File, 'r')
            for self.Word in self.FileDaTradurre:
                self.TranslatedWord = str(TradPy.translate(self.Word, self.Language, 2))
                self.ResultFile.append(self.TranslatedWord)
            self.FileDaTradurre.close()
            self.TestoTradotto = string.join(self.ResultFile, "")
            self.ResultFile = []
            self.FileTradotto = open(self.PathOutput, 'w')
            self.FileTradotto.write(self.TestoTradotto)
            self.FileTradotto.close()
        #----------------------------------------------------------------------


    def main(self):
        #--- The Noledge ------------------------------------------------------
        self.Noledge = ['Cifrario di Cesare',
                        'Alfabeto Carbonaro',
                        'Cifrario di Albam',
                        'Codifica Atbash',
                        'Rot-13']
        #----------------------------------------------------------------------

        #--- Populate the ListBox----------------------------------------------
        for self.Element in self.Noledge:
            self.ListStore_1.append([self.Element])
        #----------------------------------------------------------------------

        #--- Start the loop ---------------------------------------------------
        gtk.main()
        #----------------------------------------------------------------------

#--- Start Program ------------------------------------------------------------
if __name__ == '__main__':
    CryptoPython = CryptoPython()
    CryptoPython.main()
#------------------------------------------------------------------------------
=======
import gtk
import pygtk 
import random 
import time
import string 
import pango

Noledge = ['Cifrario di Cesare', 'Alfabeto "Pizzini"', 'Alfabeto Morse', 'Alfabeto Carbonaro', 'Cifrario di Albam', 'Rot-13']


class CryptoPython():
	
	
	def __init__(self):
		self.FromBuffer = gtk.TextBuffer() 
		self.FromBuffer.set_text('Inserisci il testo da tradurre') 
		self.ToBuffer = gtk.TextBuffer()
		self.ToBuffer.set_text('Quì verrà mostrata la traduzione')
		self.From = gtk.TextView(self.FromBuffer) 
		self.From.set_wrap_mode(gtk.WRAP_CHAR) 
		self.From.connect("focus-in-event", self.DeleteText) 
		self.To = gtk.TextView(self.ToBuffer)
		self.To.set_wrap_mode(gtk.WRAP_CHAR)
		self.ScrollTo = gtk.ScrolledWindow() 
		self.ScrollTo.show()
		self.ScrollTo.add(self.To) 
		self.ScrollFrom = gtk.ScrolledWindow()
		self.ScrollFrom.add(self.From)
		self.ScrollFrom.show()
		self.List = gtk.ListStore(str) 
		for self.c in Noledge:
			self.List.append([self.c]) 
		self.SelectCombo = gtk.ComboBoxEntry(self.List, 0) 
		self.SelectCombo.grab_focus()
		self.EntryFile = gtk.Entry()
		self.EntryFile.set_size_request(100, 27)
		self.SelectFile = gtk.Button('File')
		self.SelectFile.connect("clicked", self.OpenFile)
		self.SelectFile.set_usize(80, 10)
		self.Cryptize = gtk.Button('Codifica') 
		self.Cryptize.connect("clicked", self.Codifica)
		self.Cryptize.set_usize(0, 0)
		self.DeCryptize = gtk.Button('Decodifica')
		self.DeCryptize.connect("clicked", self.Decodifica)
		self.DeCryptize.set_usize(0, 0)
		self.Hbox1 = gtk.HBox(homogeneous = False)
		self.Hbox1.set_size_request(10, 20)
		self.Hbox1.pack_start(self.SelectCombo)
		self.Hbox2 = gtk.HBox(homogeneous = False)
		self.Hbox2.pack_start(self.EntryFile)
		self.Hbox2.pack_start(self.SelectFile,expand = False)
		self.Hbox2.set_usize(0, 10)
		self.Vbox1 = gtk.VBox(homogeneous = False)
		self.Vbox1.set_size_request(20, 10)
		self.Vbox1.pack_start(self.Hbox1)
		self.Vbox1.pack_start(gtk.Label(''))
		self.Vbox1.pack_start(self.Hbox2)
		self.Vbox1.pack_start(self.Cryptize)
		self.Vbox1.pack_start(self.DeCryptize)
		self.Hbox3 = gtk.HBox(homogeneous = False)
		self.Hbox4 = gtk.HBox(homogeneous = False)
		self.Hbox4.set_size_request(450, 200)
		self.Hbox4.pack_start(self.ScrollFrom)
		self.Hbox4.pack_start(self.Vbox1)
		self.Hbox4.pack_start(self.ScrollTo)
		self.Hbox5 = gtk.HBox(homogeneous = False)
		self.Vbox2 = gtk.VBox(homogeneous = False)
		self.Vbox2.pack_start(self.Hbox3)
		self.Vbox2.pack_start(self.Hbox4)
		self.Vbox2.pack_start(self.Hbox5)
		self.Window = gtk.Window()
		self.Window.set_size_request(800, 200)
		self.Window.set_border_width(5)
		self.Window.set_resizable(gtk.FALSE)
		self.Window.add(self.Vbox2)
		self.Window.set_focus(None)
		self.Window.show_all()


	def DeleteText(self, From, FromBuffer):
		self.FromBuffer.set_text('')

	
	def OpenFile(self, SelectFile):
		self.openfile = gtk.FileChooserDialog(title = "Open file .txt", action = 0, buttons = (gtk.STOCK_OPEN, 3))
		self.openfile.set_default_response(0)
		return_file = self.openfile.run()
		if return_file == 3:
			return_file = self.openfile.get_filename()
			self.EntryFile.set_text(str(return_file))
			self.openfile.destroy()
		
   
	def Codifica(self, Cryptize):
		
		self.Cesare = {'A':'D','a':'D', 'B':'E', 'b':'E', 'C':'F', 'c':'F', 'D':'G', 'd':'G', 'E':'H', 'e':'H', 'F':'I', 'f':'I', 'G':'L', 'g':'L', 'H':'M', 'h':'M', 'I':'N', 'i':'N', 'L':'O', 'l':'O', 'M':'P', 'm':'P', 'N':'Q', 'n':'Q', 'O':'R', 'o':'R', 'P':'S', 'p':'S', 'Q':'T', 'q':'T', 'R':'U', 'r':'U', 'S':'V', 's':'V', 'T':'Z', 't':'Z', 'U':'A', 'u':'A', 'V':'B', 'v':'B', 'Z':'C', 'z':'C', ' ':' ', '1':'4', '2':'5', '3':'6', '4':'7', '5':'8', '6':'9', '7':'0', '8':'1', '9':'2', '0':'3' }

		self.Pizzini = {' ':' ', 'a':'4', 'A':'4', 'b':'5', 'B':'5', 'c':'6', 'C':'6', 'd':'7', 'D':'7', 'e':'8', 'E':'8', 'f':'9','F':'9', 'g':'10', 'G':'10', 'h':'11', 'H':'11', 'i':'12', 'I':'12', 'l':'13', 'L':'13', 'm':'14', 'M':'14', 'n':'15', 'N':'14', 'o':'16', 'O':'16', 'p':'17', 'P':'17', 'q':'18', 'Q':'19', 'r':'20', 'R':'20', 's':'21', 'S':'21', 't':'22', 'T':'22', 'u':'23', 'U':'23', 'v':'24', 'V':'24', 'z':'25', 'Z':'25'}
		
		self.Morse = {"a": "·-", "b": "-···", "c":  "-·-·", "d": "-··" , "e": "·", "f": "··-·", "g": "--·", "h": "····", "i": "··", "l": "·-··", "m": "--", "n": "-·", "o": "---", "p": "·--·", "q": "--·-", "r": "·-·", "s": "···", "t": "-" , "u": "··-", "v": "···-", "z": "--··", " ": "###", "k": "-·-", "j": "·---","w": "·--", "y": "-·--" , "x": "-··-", "0": "-----", "1": "·----", "2": "··---", "3": "···--", "4": "····-", "5": "·····", "6": "-····", "7": "--···", "8": "---··", "9":"----·", ".": "·-·-·-" , ",": "--··--", ":": "---···", "?": "··--··", "!": "-·-·--" , "'": "·----.", "\"": "·-··-·" }
		
		self.Carbonaro = {'a':'o', 'b':'p', 'c':'g', 'd':'t', 'e':'i', 'f':'v', 'g':'c', 'h':'h', 'i':'e', 'l':'r', 'm':'n', 'n':'m', 'o':'a', 'p':'b', 'q':'q', 'r':'l', 's':'z', 't':'d', 'u':'u', 'v':'f', 'z':'s'} 
		
		self.Albam = {'q':'b', 'w':'h', 'e':'p', 'r':'c', 't':'e', 'y':'j', 'u':'f', 'i':'t', 'o':'z', 'p':'a', 'a':'l', 's':'d', 'd':'o', 'f':'q', 'g':'r', 'h':'s', 'j':'u', 'k':'v', 'l':'w', 'z':'k', 'x':'i', 'c':'n', 'v':'g', 'b':'m', 'n':'y', 'm':'x'} 
		
		self.Rot13 = {'q':'d', 'w':'j', 'e':'r', 'r':'e', 't':'g', 'y':'l', 'u':'h', 'i':'v', 'o':'b', 'p':'c', 'a':'n', 's':'f', 'd':'q', 'f':'s', 'g':'t', 'h':'u', 'j':'w', 'k':'x', 'l':'y', 'z':'m', 'x':'k', 'c':'p', 'v':'i', 'b':'o', 'n':'a', 'm':'z'} 
		
		self.Ls = {}
		self.Result = []
		self.ResultFile = []
		self.Select = None
		self.Language = str(self.SelectCombo.get_active_text())
		self.File = str(self.EntryFile.get_text())
		self.Path = []
		self.Buffer = self.From.get_buffer()
		self.start, self.end = self.Buffer.get_bounds()
		self.Testo = str(self.Buffer.get_text(self.start, self.end))
		
		if self.Language == 'Cifrario di Cesare':
			self.Ls = self.Cesare
		if self.Language == 'Alfabeto "Pizzini"':
			self.Ls = self.Pizzini
		if self.Language == 'Alfabeto Morse':
			self.Ls = self.Morse
		if self.Language == 'Alfabeto Carbonaro':
			self.Ls = self.Carbonaro
		if self.Language == 'Cifrario di Albam':
			self.Ls = self.Albam
		if self.Language == 'Rot-13':
			self.Ls = self.Rot13
		
		if self.File != "":
			try:
				self.PathSplittata = string.split(self.File, "/")
				for self.Key in self.PathSplittata:
					self.Path.append(self.Key)
				del self.Path[-1]
				self.PathCompleta = string.join(self.Path, "/")
				self.PathOutput = self.PathCompleta+"/output.txt"
			except:
				pass
			if self.Language != 'Alfabeto Morse':
				try:
					self.FileDaTradurre = open(self.File, 'r')
					for self.Parole in self.FileDaTradurre:
						for self.Lettere in self.Parole:
							try:
								self.y = self.Ls[self.Lettere]
								self.ResultFile.append(self.y)
							except:
								self.ResultFile.append(self.Lettere)
					self.FileDaTradurre.close()
					self.TestoTradotto = string.join(self.ResultFile, "")
					self.FileTradotto = open(self.PathOutput, 'w')
					self.FileTradotto.write(self.TestoTradotto)
					self.FileTradotto.close()
				except:
					pass
			if self.Language == 'Alfabeto Morse':
				self.FileDaTradurre = open(self.File, 'r')
				for self.Parole in self.FileDaTradurre:
					for self.Lettere in self.Parole:
						try:
							self.y = self.Ls[self.Lettere]
							self.ResultFile.append(self.y)
						except:
							self.ResultFile.append(self.Lettere)
				self.FileDaTradurre.close()
				self.TestoTradotto = string.join(self.ResultFile, "/")
				self.FileTradotto = open(self.PathOutput, 'w')
				self.FileTradotto.write(self.TestoTradotto)
				self.FileTradotto.close()
		
		if self.Testo != "":
			if self.Language != 'Alfabeto Morse':
				for self.x in self.Testo:
					try:
						self.y = self.Ls[self.x]
						self.Result.append(self.y)
					except:
						self.Result.append(self.x)
				self.Traduct = string.join(self.Result, "")
				self.ToBuffer.set_text(str(self.Traduct))
			if self.Language == 'Alfabeto Morse':
				for self.x in self.Testo:
					try:
						self.y = self.Ls[self.x]
						self.Result.append(self.y)
					except:
						self.Result.append(self.x)
				self.Traduct = string.join(self.Result, "/")
				self.ToBuffer.set_text(str(self.Traduct))


	def Decodifica(self, DeCryptize,):
		
		self.Cesare = {'A':'d','a':'D', 'B':'e', 'b':'E', 'C':'f', 'c':'F', 'D':'g', 'd':'G', 'E':'h', 'e':'H', 'F':'i', 'f':'I', 'G':'l', 'g':'L', 'H':'m', 'h':'M', 'I':'n', 'i':'N', 'L':'o', 'l':'O', 'M':'p', 'm':'P', 'N':'q', 'n':'Q', 'O':'r', 'o':'R', 'P':'s', 'p':'S', 'Q':'t', 'q':'T', 'R':'u', 'r':'U', 'S':'v', 's':'V', 'T':'z', 't':'Z', 'U':'a', 'u':'A', 'V':'b', 'v':'B', 'Z':'c', 'z':'C', ' ':' ', '1':'4', '2':'5', '3':'6', '4':'7', '5':'8', '6':'9', '7':'0', '8':'1', '9':'2', '0':'3' }
		
		self.Pizzini = {'111':' ','a':'4', 'A':'4', 'b':'5', 'B':'5', 'c':'6', 'C':'6', 'd':'7', 'D':'7', 'e':'8', 'E':'8', 'f':'9','F':'9', 'g':'010', 'G':'010', 'h':'011', 'H':'011', 'i':'012', 'I':'012', 'l':'013', 'L':'013', 'm':'014', 'M':'014', 'n':'015', 'N':'014', 'o':'016', 'O':'016', 'p':'017', 'P':'017', 'q':'018', 'Q':'019', 'r':'020', 'R':'020', 's':'021', 'S':'021', 't':'022', 'T':'022', 'u':'023', 'U':'023', 'v':'024', 'V':'024', 'z':'025', 'Z':'025'}
		
		self.Morse = {"a": "·-", "b": "-···", "c":  "-·-·", "d": "-··" , "e": "·", "f": "··-·", "g": "--·", "h": "····", "i": "··", "l": "·-··", "m": "--", "n": "-·", "o": "---", "p": "·--·", "q": "--·-", "r": "·-·", "s": "···", "t": "-" , "u": "··-", "v": "···-", "z": "--··", " ": "###", "k": "-·-", "j": "·---","w": "·--", "y": "-·--" , "x": "-··-", "0": "-----", "1": "·----", "2": "··---", "3": "···--", "4": "····-", "5": "·····", "6": "-····", "7": "--···", "8": "---··", "9":"----·", ".": "·-·-·-" , ",": "--··--", ":": "---···", "?": "··--··", "!": "-·-·--" , "'": "·----.", "\"": "·-··-·" }
		
		self.Carbonaro = {'a':'o', 'b':'p', 'c':'g', 'd':'t', 'e':'i', 'f':'v', 'g':'c', 'h':'h', 'i':'e', 'l':'r', 'm':'n', 'n':'m', 'o':'a', 'p':'b', 'q':'q', 'r':'l', 's':'z', 't':'d', 'u':'u', 'v':'f', 'z':'s'} 
		
		self.Albam = {'q':'b', 'w':'h', 'e':'p', 'r':'c', 't':'e', 'y':'j', 'u':'f', 'i':'t', 'o':'z', 'p':'a', 'a':'l', 's':'d', 'd':'o', 'f':'q', 'g':'r', 'h':'s', 'j':'u', 'k':'v', 'l':'w', 'z':'k', 'x':'i', 'c':'n', 'v':'g', 'b':'m', 'n':'y', 'm':'x'} 
		
		self.Rot13 = {'q':'d', 'w':'j', 'e':'r', 'r':'e', 't':'g', 'y':'l', 'u':'h', 'i':'v', 'o':'b', 'p':'c', 'a':'n', 's':'f', 'd':'q', 'f':'s', 'g':'t', 'h':'u', 'j':'w', 'k':'x', 'l':'y', 'z':'m', 'x':'k', 'c':'p', 'v':'i', 'b':'o', 'n':'a', 'm':'z'} 
		
		self.Result = []
		self.ResultFile = []
		self.Path = []
		self.Ls = {}
		self.ReverseLs = {}
		self.Support = '0'
		self.Language = str(self.SelectCombo.get_active_text())
		self.Buffer = self.From.get_buffer()
		self.start, self.end = self.Buffer.get_bounds()
		self.File = str(self.EntryFile.get_text())
		self.Testo = str(self.Buffer.get_text(self.start, self.end))
		
		if self.Language == 'Cifrario di Cesare':
			self.Ls = self.Cesare
		if self.Language == 'Alfabeto "Pizzini"':
			self.Ls = self.Pizzini
		if self.Language == 'Alfabeto Morse':
			self.Ls = self.Morse
		if self.Language == 'Alfabeto Carbonaro':
			self.Ls = self.Carbonaro
		if self.Language == 'Cifrario di Albam':
			self.Ls = self.Albam
		if self.Language == 'Rot-13':
			self.Ls = self.Rot13
		
		self.Lsk = self.Ls.keys()
		self.Lsv = self.Ls.values()
		
		for self.i in range(len(self.Ls)):
			self.ReverseLs[self.Lsv[self.i]] = self.Lsk[self.i] 
		
		if self.File != "":
			try:
				self.PathSplittata = string.split(self.File, "/")
				for self.Key in self.PathSplittata:
					self.Path.append(self.Key)
				del self.Path[-1]
				self.PathCompleta = string.join(self.Path, "/")
				self.PathOutput = self.PathCompleta+"/output.txt"
			except:
				pass
			try:
				self.FileDaTradurre = open(self.File, 'r')
				if self.Language != 'Alfabeto "Pizzini"' and self.Language != 'Alfabeto Morse':
					for self.Parole in self.FileDaTradurre:
						for self.Lettere in self.Parole:
							try:
								self.y = self.ReverseLs[self.Lettere]
								self.ResultFile.append(self.y)
							except:
								self.Result.append(self.Lettere)
					self.FileDaTradurre.close()
					self.TestoTradotto = string.join(self.ResultFile, "")
					self.FileTradotto = open(self.PathOutput, 'w')
					self.FileTradotto.write(self.TestoTradotto)
					self.FileTradotto.close()
						
				if self.Language == 'Alfabeto "Pizzini"':
					for self.Parole in self.FileDaTradurre:
						for self.Lettere in self.Parole:
							try:
								if str(self.Lettere) == ' ':
									self.Lettere = '111'
									self.ResultFile.append(self.Pizzini[self.Lettere])
								if str(self.Lettere) == '0':
									if str(self.Support) != '0':
										self.Support = self.Support + self.Lettere
								if str(self.Lettere) == '1':
									self.Support = self.Support+self.Lettere
								if str(self.Lettere) == '2':
									self.Support = self.Support+self.Lettere
								if str(self.Lettere) == '3':
									self.Support =self.Support+self.Lettere
								if int(self.Lettere) > 2:
									if int(self.Support) != 0:
										self.Support = self.Support+self.Lettere
										self.Lettere = 0
								if int(self.Support) < 30 and int(self.Support) > 9:
										self.ResultFile.append(self.ReverseLs[self.Support])
								if int(self.Support) > 9:
										self.Support = '0'
								if int(self.Lettere) > 3 and int(self.Lettere) != 111:
									self.ResultFile.append(self.ReverseLs[self.Lettere])
							except:
								self.ResultFile.append(self.Lettere)
					self.FileDaTradurre.close()
					self.TestoTradotto = string.join(self.ResultFile, "")
					self.FileTradotto = open(self.PathOutput,'w')
					self.FileTradotto.write(self.TestoTradotto)
					self.FileTradotto.close()
						
				if self.Language == 'Alfabeto Morse':
					try:
						self.TestoSplittato = string.split(self.FileDaTradurre, "/")
						for self.Lettere in self.TestoSplittato:
							self.ResultFile.append(self.ReverseLs[self.Lettere])
					except:
						self.ResultFile.append(self.Lettere)
					self.FileDaTradurre.close()
					self.TestoTradotto = string.join(self.ResultFile, "")
					self.FileTradotto = open(self.PathOutput, 'w')
					self.FileTradotto.write(self.TestoTradotto)
					self.FileTradotto.close()
			except:
				pass
					
		if self.Testo != "":
			try:
				if self.Language != 'Alfabeto "Pizzini"' and self.Language != 'Alfabeto Morse':
					for self.x in self.Testo:
						try:
							self.y = self.ReverseLs[self.x]
							self.Result.append(self.y)
						except:
							self.Result.append(self.x)
					self.Traduct = string.join(self.Result, "")
					self.ToBuffer.set_text(str(self.Traduct))
				if self.Language == 'Alfabeto "Pizzini"':
					for self.x in self.Testo:
						if str(self.x) == ' ':
							self.x = '111'
							self.Result.append(self.Pizzini[self.x])
						if str(self.x) == '0':
							if str(self.Support) != '0':
								self.Support = self.Support + self.x
						if str(self.x) == '1':
							self.Support = self.Support+self.x
						if str(self.x) == '2':
							self.Support = self.Support+self.x
						if str(self.x) == '3':
							self.Support =self.Support+self.x
						if int(self.x) > 2:
							if int(self.Support) != 0:
								self.Support = self.Support+self.x
								self.x = 0
						if int(self.Support) < 30 and int(self.Support) > 9:
								self.Result.append(self.ReverseLs[self.Support])
						if int(self.Support) > 9:
								self.Support = '0'
						if int(self.x) > 3 and int(self.x) != 111:
							self.Result.append(self.ReverseLs[self.x])
					self.Traduct = string.join(self.Result, "")
					self.ToBuffer.set_text(str(self.Traduct))
				if self.Language == 'Alfabeto Morse':
					self.TestoSplittato = string.split(self.Testo, "/")
					for self.x in self.TestoSplittato:
						self.Result.append(self.ReverseLs[self.x])
					self.Traduct = string.join(self.Result, "")
					self.ToBuffer.set_text(str(self.Traduct))
			except:
				pass


	def main(self):
		gtk.main() 


if __name__ == '__main__': 
	CryptoPython = CryptoPython()
	CryptoPython.main()
>>>>>>> 678493e9424dc7604dba4331529bd1a338d0f0f9
