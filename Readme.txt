Commit:  

- 05.07.2012 - "Creato primo abbozzo del Programma, supporta solo il Cifrari di Cesare"
- 05.07.2012 - "Created first version of the program it supports just the Caesar Cipher"
- 06.07.2012 - "Aggiunta una gtk.Entry con completamento automatico per la scelta del cifrario"
- 06.07.2012 - "Added a gtk.Entry with autocomplete for choice the cipher"
- 06.07.2012 - "Sostituita la gtk.Entry ritenuta complicata a favore di una gtk.ComboBox"
- 06.07.2012 - "Replaced the gtk.Entry with a gtk.ComboBox, Cause: I think is very difficult to use"
- 07.07.2012 - "Aggiunto un sistema di cancellazione contenuto della prima TextBox"
- 07.07.2012 - "Aggiunto sistema di *A Capo* automatico"
- 07.07.2012 - "Added automatic newline system"
- 07.07.2012 - "Added a system for delete the TextBox's content"
- 07.07.2012 - "Modificata la diposizione dei Widget"
- 07.07.2012 - "Modified the Widget's Layout"
- 07.07.2012 - "Aggiunto nuovo cifrario"
- 07.07.2012 - "Added a new cipher"
- 08.07.2012 - "BugFix: {05.07.2012-06.07.2012}"
- 09.07.2012 - "BugFix: {07.07.2012}"
- 09.07.2012 - "Uppate le modifiche effettuate su {http://bitbucket.org/Develop4Ever/cryptopython}"
- 09.07.2012 - "Changes were uploaded" 
- 10.07.2012 - "Aggiunta la codifica e la decodifica dell' Alfabeto Morse"
- 10.07.2012 - "Adding encoding and decoding of 'Morse"
- 13.07.2012 - "Aggiunte nuovi cifrari"
- 13.07.2012 - "Adding new cipher"
- 13.07.2012 - "Aggiunta una nuova opzione: upload di file da codificare o decodificare"
- 13.07.2012 - "Adding a new function: you can upload a file for coding or decoding"
- 14.07.2012 - "Effettuate piccole modifiche grafiche"
- 14.07.2012 - "Made a little graphic modify"
- 15.07.2012 - "Quasi finito, creato pacchetto .Deb"
- 15.07.2012 - "Almost done, created a Debian packages"
- 15.07.2012 - "Uppato il progetto finito"
- 15.07.2012 - "Uploaded the complete project"

Bug:

- 05.07.2012 - "Pressione pulsante x provoca il crash...del Pc"
               "Alla seconda pressione del bottone di codifica si verifica un Freeze"
               "Se si lacia vuoto il campo di immissione testo si verifica un Freeze"

- 06.07.2012 - "ComboBox non funziona...restituisce sempre valore 0..."
               "Il sistema di andatura a capo automatico non funziona..."
               
- 07.07.2012 - "Si verifica un problema nella decodifica del 'Codice Pizzini'"

- 13.07.2012 - "Errori nella decodifica dei file"

Version:

- 05.07.2012 - "Version 1.0"
- 07.07.2012 - "Version 1.2"
- 09.07.2012 - "Version 2.1"
- 10.07.2012 - "Version 2.2"
- 15.07.2012 - "Version 3.0"


